import apache_beam as beam

from apache_beam.options.pipeline_options import PipelineOptions

if __name__ == '__main__':
    project = 'mm-dwh-dev'
    dataset = 'ZZ_APE'
    source_table = project + ':' + dataset + '.' + 'mock_input_dataflow'
    output_table = project + ':' + dataset + '.' + 'mock_output_dataflow'
    output_table_schema = 'first_name:STRING, last_name:STRING, first_exam:INTEGER, second_exam:INTEGER, pass:BOOLEAN'

    options = PipelineOptions(
        service_account_email='dataflow@mm-dwh-dev.iam.gserviceaccount.com',
        flags='',
        runner='DataflowRunner',
        project=project,
        job_name='testing-dataflow',
        temp_location='gs://mm-dwh-dev-dataflow/tmp',
        region='europe-west1')

    with beam.Pipeline(options=options) as p:
        (p | 'ReadTable' >> beam.io.ReadFromBigQuery(table=source_table)
           | 'Pass' >> beam.Map(lambda row: {'first_name': row['first_name'], 'last_name': row['last_name'], 'first_exam': row['first_exam'],
                                             'second_exam': row['second_exam'], 'pass': ((row['first_exam'] + row['second_exam'])/2.0) >= 5})
           | 'WriteTable' >> beam.io.WriteToBigQuery(
                    output_table,
                    schema=output_table_schema,
                    write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE,
                    create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED)
         )
